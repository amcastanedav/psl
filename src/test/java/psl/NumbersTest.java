package psl;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by angelica on 21/11/16.
 */
public class NumbersTest {

    Random random = new Random();
    Numbers numbers = new Numbers();
    Print print = new Print();

    @Test
    public void testOne() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.one(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testTwo() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.two(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testThree() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.three(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testFour() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.four(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testFive() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.five(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testSix() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.six(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testSeven() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.seven(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testEight() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.eight(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testNine() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.nine(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testZero() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = numbers.zero(size);
        Assert.assertEquals(size*2 + 3, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }
}
