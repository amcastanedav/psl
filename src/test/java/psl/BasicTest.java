package psl;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by angelica on 21/11/16.
 */
public class BasicTest {

    Random random = new Random();
    Basic basic = new Basic();
    Print print = new Print();

    @Test
    public void verticalLineRight() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = basic.verticalLine(size, true);
        Assert.assertEquals(size, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size-1).size());
        Assert.assertEquals(basic.space, result.get(random.nextInt(size)).get(0));
        Assert.assertEquals(basic.vertical, result.get(random.nextInt(size)).get(size+1));
        print.print(result);
    }

    @Test
    public void verticalLineLeft() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = basic.verticalLine(size, false);
        Assert.assertEquals(size, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size-1).size());
        Assert.assertEquals(basic.vertical, result.get(random.nextInt(size)).get(0));
        Assert.assertEquals(basic.space, result.get(random.nextInt(size)).get(size+1));
        print.print(result);
    }

    @Test
    public void doubleVerticalLine() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = basic.verticalDoubleLine(size);
        Assert.assertEquals(size, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size-1).size());
        Assert.assertEquals(basic.vertical, result.get(random.nextInt(size)).get(0));
        Assert.assertEquals(basic.space, result.get(random.nextInt(size)).get(random.nextInt(size)+1));
        Assert.assertEquals(basic.vertical, result.get(random.nextInt(size)).get(size+1));
        print.print(result);
    }

    @Test
    public void horizontalBlank() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<String> result = basic.horizontalBlank(size);
        Assert.assertEquals(size+2, result.size());
        Assert.assertEquals(basic.space, result.get(0));
        Assert.assertEquals(basic.space, result.get(random.nextInt(size)+1));
        Assert.assertEquals(basic.space, result.get(size+1));
        print.printHorizontal(result);
    }

    @Test
    public void horizontalLine() {
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<String> result = basic.horizontalLine(size);
        Assert.assertEquals(size+2, result.size());
        Assert.assertEquals(basic.space, result.get(0));
        Assert.assertEquals(basic.horizontal, result.get(random.nextInt(size)+1));
        Assert.assertEquals(basic.space, result.get(size+1));
        print.printHorizontal(result);
    }
}
