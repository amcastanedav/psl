package psl;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by angelica on 21/11/16.
 */
public class UpperTest {

    Random random = new Random();
    Upper upper = new Upper();
    Print print = new Print();

    @Test
    public void testLine(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = upper.line(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testFour(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = upper.four(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testRight(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = upper.right(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testLeft(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = upper.left(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testArc(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = upper.arc(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }
}
