package psl;

import java.util.Iterator;
import java.util.List;

/**
 * Created by angelica on 21/11/16.
 */
public class Print {

    public void print (List<List<String>> result){
        Iterator<List<String>> iterator = result.iterator();
        while (iterator.hasNext()){
            Iterator<String> subIterator = iterator.next().iterator();
            while (subIterator.hasNext()){
                System.out.print(subIterator.next());
            }
            System.out.println();
        }
    }

    public void printHorizontal (List<String> result){
        Iterator<String> iterator = result.iterator();
        while (iterator.hasNext()){
            System.out.print(iterator.next());
        }
    }
}
