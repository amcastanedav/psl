package psl;

import org.junit.Assert;
import org.junit.Test;

import java.util.List;
import java.util.Random;

/**
 * Created by angelica on 21/11/16.
 */
public class DownTest {

    Random random = new Random();
    Down down = new Down();
    Print print = new Print();

    @Test
    public void testLine(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = down.line(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testRight(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = down.right(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testLeft(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = down.left(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }

    @Test
    public void testArc(){
        int size = random.nextInt(10) + 1;
        System.out.println(size);
        List<List<String>> result = down.arc(size);
        Assert.assertEquals(size+1, result.size());
        Assert.assertEquals(size+2, result.get(0).size());
        Assert.assertEquals(size+2, result.get(random.nextInt(size)).size());
        Assert.assertEquals(size+2, result.get(size).size());
        print.print(result);
    }
}
