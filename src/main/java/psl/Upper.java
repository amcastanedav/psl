package psl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angelica on 21/11/16.
 */
public class Upper {

    Basic basic = new Basic();

    List<List<String>> line(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.add(basic.horizontalBlank(size));
        result.addAll(basic.verticalLine(size, true));

        return result;
    }

    List<List<String>> four(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.add(basic.horizontalBlank(size));
        result.addAll(basic.verticalDoubleLine(size));

        return result;
    }

    List<List<String>> right(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.add(basic.horizontalLine(size));
        result.addAll(basic.verticalLine(size, true));

        return result;
    }

    List<List<String>> left(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.add(basic.horizontalLine(size));
        result.addAll(basic.verticalLine(size, false));

        return result;
    }

    List<List<String>> arc(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.add(basic.horizontalLine(size));
        result.addAll(basic.verticalDoubleLine(size));

        return result;
    }
}
