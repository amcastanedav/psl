package psl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angelica on 21/11/16.
 */
public class Numbers {

    Upper upper = new Upper();
    Basic basic = new Basic();
    Down down = new Down();

    List<List<String>> one (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.line(size));
        result.add(basic.horizontalBlank(size));
        result.addAll(down.line(size));

        return result;
    }

    List<List<String>> two (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.right(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.left(size));

        return result;
    }

    List<List<String>> three (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.right(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.right(size));

        return result;
    }

    List<List<String>> four (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.four(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.line(size));

        return result;
    }

    List<List<String>> five (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.left(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.right(size));

        return result;
    }

    List<List<String>> six (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.left(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.arc(size));

        return result;
    }

    List<List<String>> seven (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.right(size));
        result.add(basic.horizontalBlank(size));
        result.addAll(down.line(size));

        return result;
    }

    List<List<String>> eight (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.arc(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.arc(size));

        return result;
    }

    List<List<String>> nine (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.arc(size));
        result.add(basic.horizontalLine(size));
        result.addAll(down.right(size));

        return result;
    }

    List<List<String>> zero (int size){
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(upper.arc(size));
        result.add(basic.horizontalBlank(size));
        result.addAll(down.arc(size));

        return result;
    }
}
