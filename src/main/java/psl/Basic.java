package psl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angelica on 21/11/16.
 */
public class Basic {

    String vertical = "|";
    String horizontal = "-";
    String space = " ";

    List<List<String>> verticalLine(int size, boolean right) {
        List<List<String>> result = new ArrayList<List<String>>();

        for (int i = 0; i < size; i++) {
            List<String> l = new ArrayList<String>();
            if (!right)
                l.add(vertical);
            for (int j = 0; j < size + 1; j++) {
                l.add(space);
            }
            if (right)
                l.add(vertical);
            result.add(l);
        }

        return result;
    }

    List<List<String>> verticalDoubleLine(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        for (int i = 0; i < size; i++) {
            List<String> l = new ArrayList<String>();
            l.add(vertical);
            for (int j = 0; j < size; j++) {
                l.add(space);
            }
            l.add(vertical);
            result.add(l);
        }

        return result;
    }

    List<String> horizontalBlank(int size) {
        List<String> blank = new ArrayList<String>();
        for (int i = 0; i < size + 2; i++) {
            blank.add(space);
        }
        return blank;
    }

    List<String> horizontalLine(int size) {
        List<String> line = new ArrayList<String>();
        line.add(space);
        for (int i = 0; i < size; i++) {
            line.add(horizontal);
        }
        line.add(space);
        return line;
    }

}
