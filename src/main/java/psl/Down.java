package psl;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by angelica on 21/11/16.
 */
public class Down {

    Basic basic = new Basic();

    List<List<String>> line(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(basic.verticalLine(size, true));
        result.add(basic.horizontalBlank(size));

        return result;
    }

    List<List<String>> right(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(basic.verticalLine(size, true));
        result.add(basic.horizontalLine(size));

        return result;
    }

    List<List<String>> left(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(basic.verticalLine(size, false));
        result.add(basic.horizontalLine(size));

        return result;
    }

    List<List<String>> arc(int size) {
        List<List<String>> result = new ArrayList<List<String>>();

        result.addAll(basic.verticalDoubleLine(size));
        result.add(basic.horizontalLine(size));

        return result;
    }
}
