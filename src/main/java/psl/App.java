package psl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        Numbers numbers = new Numbers();
        Basic basic = new Basic();
        String entry = "1,123";

        while (!entry.equalsIgnoreCase("0,0")) {

            try {
                System.out.println("Please enter the arguments to make the exercise");
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                entry = br.readLine();

                List<String> list = Arrays.asList(entry.split(","));
                if (list.size() == 2) {

                    int size = Integer.valueOf(list.get(0));
                    if (size <= 10) {
                        String stringNumbers = list.get(1).trim();

                        List<List<String>> toPrint = new ArrayList<List<String>>();
                        for (int i = 0; i < size * 2 + 3; i++) {
                            List<String> row = new ArrayList<String>();
                            toPrint.add(row);
                        }

                        for (int i = 0; i < stringNumbers.length(); i++) {
                            int digit = 10;
                            try {
                                digit = Integer.valueOf(String.valueOf(stringNumbers.charAt(i)));
                            } catch (NumberFormatException exception) {
                                System.out.println("Something wrong trying to process " + stringNumbers.charAt(i));
                            }
                            List<List<String>> digitList = new ArrayList<List<String>>();
                            switch (digit) {
                                case 1:
                                    digitList = numbers.one(size);
                                    break;
                                case 2:
                                    digitList = numbers.two(size);
                                    break;
                                case 3:
                                    digitList = numbers.three(size);
                                    break;
                                case 4:
                                    digitList = numbers.four(size);
                                    break;
                                case 5:
                                    digitList = numbers.five(size);
                                    break;
                                case 6:
                                    digitList = numbers.six(size);
                                    break;
                                case 7:
                                    digitList = numbers.seven(size);
                                    break;
                                case 8:
                                    digitList = numbers.eight(size);
                                    break;
                                case 9:
                                    digitList = numbers.nine(size);
                                    break;
                                case 0:
                                    digitList = numbers.zero(size);
                                    break;
                                default:
                                    System.out.println("Something went wrong with you arguments");
                                    break;
                            }

                            if (digitList.size() > 0) {
                                for (int j = 0; j < size * 2 + 3; j++) {
                                    toPrint.get(j).addAll(digitList.get(j));
                                    toPrint.get(j).add(basic.space);
                                }
                            }
                        }

                        Iterator<List<String>> iterator = toPrint.iterator();
                        while (iterator.hasNext()) {
                            Iterator<String> subIterator = iterator.next().iterator();
                            while (subIterator.hasNext()) {
                                System.out.print(subIterator.next());
                            }
                            System.out.println();
                        }
                    } else {
                        System.out.println("Size must be below 10");
                        entry = "1,123";
                    }
                } else {
                    System.out.println("Seems like you used more or less arguments than necessary");
                    entry = "1,123";
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }
        }
        System.out.println("Have a nice day, bye!");
    }
}
